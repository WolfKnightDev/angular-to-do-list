// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCJbwePDpelMuYihyITg2_bqxuNkty49lI",
    authDomain: "angular-to-do-list-vi.firebaseapp.com",
    databaseURL: "https://angular-to-do-list-vi.firebaseio.com",
    projectId: "angular-to-do-list-vi",
    storageBucket: "angular-to-do-list-vi.appspot.com",
    messagingSenderId: "64360015689"
  }
};
