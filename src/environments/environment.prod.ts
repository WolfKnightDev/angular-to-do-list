export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCJbwePDpelMuYihyITg2_bqxuNkty49lI",
    authDomain: "angular-to-do-list-vi.firebaseapp.com",
    databaseURL: "https://angular-to-do-list-vi.firebaseio.com",
    projectId: "angular-to-do-list-vi",
    storageBucket: "angular-to-do-list-vi.appspot.com",
    messagingSenderId: "64360015689"
  }
};
