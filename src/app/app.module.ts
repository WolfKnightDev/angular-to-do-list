import { BrowserModule } from '@angular/platform-browser';
import { NgModule, isDevMode } from '@angular/core';

import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { NgRedux, NgReduxModule, DevToolsExtension  } from '@angular-redux/store'
import { IAppState, rootReducer, INITIAL_STATE } from './store'

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoListService } from './services/todo-list.service';
import { TodoAddComponent } from './todo-add/todo-add.component';

import { environment } from '../environments/environment';
import { UserComponent } from './user/user.component';
import { AuthService } from './services/auth.service';
import { NavbarComponent } from './navbar/navbar.component';
import { SignInPanelComponent } from './auth-components/sign-in-panel/sign-in-panel.component';

import { NgxSpinnerModule } from 'ngx-spinner';
import { LoadingSpinnerService } from './services/loading-spinner.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PipeModule } from './pipesModule/PipeModule'



@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodoListComponent,
    TodoAddComponent,
    UserComponent,
    NavbarComponent,
    SignInPanelComponent

  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    NgbModule.forRoot(),
    NgReduxModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    PipeModule.forRoot()
  ],
  providers: [TodoListService, AuthService, LoadingSpinnerService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(ngRedux: NgRedux<IAppState> , private devTools:DevToolsExtension){

    let enhancers = [];
    // ... add whatever other enhancers you want.

    // You probably only want to expose this tool in devMode.
    if (isDevMode && devTools.isEnabled()) {
      enhancers = [ ...enhancers, devTools.enhancer() ];
    }

    ngRedux.configureStore(rootReducer, INITIAL_STATE,[],enhancers);
  }
}
