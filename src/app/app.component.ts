import { Component } from '@angular/core';

import { select, NgRedux } from '@angular-redux/store';
import { IAppState } from './store';
import { AppActions } from './actions';

import { Todo } from './model/todo';
import { TodoUser } from './model/todoUser';

import { AuthService } from './services/auth.service';

import { LoadingSpinnerService } from './services/loading-spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @select() user:TodoUser;

  constructor(private ngRedux: NgRedux<IAppState>, private authService: AuthService, private spinner: LoadingSpinnerService) { 
  }

  ngOnInit(){
    this.getAuth();
  }

  getAuth(){
    this.spinner.show();
    this.authService.getAuthState()
    .take(1)
    .finally(()=>this.spinner.hide())
    .subscribe(
      u => {this.ngRedux.dispatch({type:AppActions.USER_GETAUTH, user:u})},
      error => console.log("Error: ", error),
      ()=>console.log("Auth Completed!")
    );
  }

}
