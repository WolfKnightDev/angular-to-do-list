import { Component, OnInit } from '@angular/core';

import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../../store';
import { AppActions } from '../../actions';

import { AuthService } from '../../services/auth.service';

import { LoadingSpinnerService } from '../../services/loading-spinner.service';


@Component({
  selector: 'app-sign-in-panel',
  templateUrl: './sign-in-panel.component.html',
  styleUrls: ['./sign-in-panel.component.scss']
})
export class SignInPanelComponent implements OnInit {

  constructor(private ngRedux:NgRedux<IAppState>, private authService: AuthService, private spinner: LoadingSpinnerService) { }

  ngOnInit() {
  }

  signIn(){
    this.authService.loginWithGoogle()
    .then(u=>this.ngRedux.dispatch({type:AppActions.USER_SIGNIN}))
    .then(u=>this.getAuth());
  }

  getAuth(){
    this.spinner.show();
    this.authService.getAuthState()
    .take(1)
    .finally(()=>this.spinner.hide())
    .subscribe(
      u => {this.ngRedux.dispatch({type:AppActions.USER_GETAUTH, user:u})},
      error => console.log("Error: ", error),
      ()=>console.log("Auth Completed!")
    );
  }
}
