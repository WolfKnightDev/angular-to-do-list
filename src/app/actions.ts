export enum AppActions{
	ADD_TODO = 'ADD_TODO',
    REMOVE_TODO = 'REMOVE_TODO',
    TOGGLE_TODO = 'TOGGLE_TODO',
    UPDATE_TODO = 'UPDATE_TODO',
    EDIT_TODO = 'EDIT_TODO',
    CANCEL_EDIT_TODO = "CANCEL_EDIT_TODO",
    GET_TODOES = 'GET_TODOES',

    USER_GETAUTH = 'USER_GETAUTH',
    USER_SIGNIN = 'USER_SIGNIN',
    USER_SIGNOUT = 'USER_SIGNOUT'
}

/*
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const EDIT_TODO = 'EDIT_TODO';
export const GET_TODOES = 'GET_TODOES';

export const USER_GETAUTH = 'USER_GETAUTH'
export const USER_SIGNIN = 'USER_SIGNIN';
export const USER_SIGNOUT = 'USER_SIGNOUT';
*/