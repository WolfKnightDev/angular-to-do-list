export interface TodoUser {
    uid:string;
    email:string;
    displayName:string;
    photoURL:string;
    lastLoginDate: Date;
}

