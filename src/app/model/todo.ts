export interface Todo {
    id?:string;
    title:string;
    text:string;
    status:number;
    dateCreated:Date;
    lastUpdated:Date;
    uid:string;
}

