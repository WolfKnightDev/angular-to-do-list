import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../store';
import { AppActions } from '../actions'

import { Todo } from '../model/todo';
import { TodoListService } from '../services/todo-list.service';

import { User } from 'firebase/app';
import { TodoUser } from '../model/todoUser';
import { Observable } from '@firebase/util';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.scss']
})

export class TodoAddComponent implements OnInit {
  @select() user:Observable<TodoUser>;

  reactiveForm:FormGroup;

  isShowed:boolean = false;

  constructor(private ngRedux: NgRedux<IAppState>, private fb:FormBuilder, private todoListService:TodoListService) { }
  
  ngOnInit() {
    this.initForm();
  }
  
  //component methods
  showForm(){
    this.isShowed = !this.isShowed;
  }

  addTodo(todo: Todo):void{ 
    this.todoListService.addTodo(todo)
    .then(ref=>
       this.ngRedux.dispatch({type: AppActions.ADD_TODO})
    );
  }

  //Form methods
  onSubmit(){
    const controls = this.reactiveForm.controls;

    if(this.reactiveForm.invalid){
      Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched())
      return;
    }
    
    this.user.subscribe(u=>{
      const newTodo:Todo = {
        title:controls['title'].value,
        text:controls['description'].value,
        status:0,
        dateCreated:new Date(),
        lastUpdated:new Date(),
        uid:u.uid
      }
      this.addTodo(newTodo);
    })
    
    this.reactiveForm.reset()
  }

  initForm(){
    this.reactiveForm = this.fb.group({
      title:[null,[
        Validators.required
      ]
    ],
      description:[null]
    });
  }

  //Don't used now but need to implement some errors instead of native html
  isControlInvalid(controlName: string): boolean {
      const control = this.reactiveForm.controls[controlName];
      const result = control.invalid && control.touched;   
      return result;
    }

}
