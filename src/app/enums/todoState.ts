enum TodoState{
    Undefined = 0,
    Completed = 1,
    Uncompleted = 2
}