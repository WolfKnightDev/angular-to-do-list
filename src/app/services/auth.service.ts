import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app'

import { NgRedux, select } from '@angular-redux/store';

import { User } from 'firebase/app';

import { Observable } from 'rxjs/Observable'
import { of } from 'rxjs/Observable/of'
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/take'

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore'
import { TodoUser } from '../model/todoUser';
import { AppActions } from '../actions';
import { IAppState } from '../store';

@Injectable()
export class AuthService {
  constructor(private afAuth: AngularFireAuth, private db:AngularFirestore) {
   }

  private oAuthLogin(provider){
    return this.afAuth.auth.signInWithPopup(provider)
    .then((credential)=>this.updateUserData(credential.user))
    .then(u=> this.getAuthState())
  }

  loginWithGoogle(){
    const provider = new firebase.auth.GoogleAuthProvider()
    provider.setCustomParameters({
      prompt: 'select_account'
    });
    
    return this.oAuthLogin(provider);
  }

  signOut():Promise<any>{
    return this.afAuth.auth.signOut();
   
  }

  private updateUserData(user:User){
    const userRef: AngularFirestoreDocument<TodoUser> = this.db.doc(`users/${user.uid}`);

    var data = this.setSignInTodoUserModel(user);

    return userRef.set(data, {merge: true});
  }

  private setSignInTodoUserModel(user:User): TodoUser{
    if(user){
      const data: TodoUser = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
        lastLoginDate: new Date()
      }
      return data;
    }
  }

  getAuthState():Observable<TodoUser>{

    var authUser = this.afAuth.authState
    .switchMap(u => {
      if(u){
        return this.db.collection("users").doc(u.uid).valueChanges() as Observable<TodoUser>;
      }else{
        return Observable.of(null) as Observable<TodoUser>;
      }
    })
    
    return authUser;
  }
}
