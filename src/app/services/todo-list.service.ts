import { Injectable } from '@angular/core';

import { Todo } from '../model/todo';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { map } from 'rxjs/operators'
import { AngularFirestore, AngularFirestoreCollection,AngularFirestoreDocument } from 'angularfire2/firestore'
import { DocumentReference } from '@firebase/firestore-types';
import { TodoUser } from '../model/todoUser';

@Injectable()
export class TodoListService {

  constructor(private db: AngularFirestore) { 
  }
  todoCollectionName:string ='todoes';
 
  getTodoes(): Observable<Todo[]>{

    return this.db
    .collection(this.todoCollectionName, ref =>ref.orderBy('dateCreated','desc'))
    .snapshotChanges()
    .map(todoes=>{
      return todoes.map(todo=>{
        const data = todo.payload.doc.data() as Todo;
        data.id = todo.payload.doc.id;
        return data;
      })
    });
  }

  getTodoes_UID(uid:string): Observable<Todo[]>{
    return this.db
    .collection(this.todoCollectionName, ref =>ref.where("uid","==",uid).orderBy('status','asc').orderBy('lastUpdated','desc'))
    .snapshotChanges()
    .map(todoes=>{
      return todoes.map(todo=>{
        const data = todo.payload.doc.data() as Todo;
        data.id = todo.payload.doc.id;
        return data;
      })
    });
  }
  
  addTodo(todo:Todo): Promise<DocumentReference>{
   return this.db.collection(this.todoCollectionName).add(todo);
  }

  deleteTodo(todo:Todo): Promise<void>{
    return this.db.collection(this.todoCollectionName).doc(todo.id).delete();
  }

  updateTodo(todo:Todo): Promise<void>{
    return this.db.collection(this.todoCollectionName).doc(todo.id).update(todo);
  }
}
