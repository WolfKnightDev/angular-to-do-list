import { Component, OnInit } from '@angular/core';

import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../store';
import { AppActions} from '../actions'

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/take'

import { AuthService } from '../services/auth.service';
import { TodoUser } from '../model/todoUser';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @select() todoesCount:number;
  @select() completedCount:number;
  @select() user:TodoUser;

  constructor(private ngRedux: NgRedux<IAppState>, private authService: AuthService) { }

  ngOnInit() {
    
  }

  signOut(){
    this.authService.signOut()
    .then(resp=>
        this.ngRedux.dispatch({type:AppActions.USER_SIGNOUT})
    )
  }
}
