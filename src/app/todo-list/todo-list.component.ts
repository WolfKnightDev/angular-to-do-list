import { Component, OnInit,Pipe,PipeTransform, Input } from '@angular/core';


import { NgRedux, select, select$ } from '@angular-redux/store';
import { IAppState } from '../store';
import { AppActions } from '../actions'

import { Todo } from '../model/todo';
import { TodoListService } from '../services/todo-list.service'
import { Observable } from '@firebase/util';
import { TodoUser } from '../model/todoUser';



@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
  
})
export class TodoListComponent implements OnInit {
  @select() user:Observable<TodoUser>;
  @select() todoes:Todo[];
  @select() activeCount:number;
  @select() completedCount:number;
  

  constructor(private ngRedux:NgRedux<IAppState> ,private todoListService: TodoListService) { }

  ngOnInit() {
   
  }

  ngAfterViewInit(){
    this.user.subscribe(u=>{
      if(u != null)
      this.getTodoes_UID(u.uid);
    });
  }
  getTodoes():void{
    this.todoListService.getTodoes()
      .subscribe(todoesData => this.ngRedux.dispatch({type: AppActions.GET_TODOES, todoes:todoesData }));
  }

  getTodoes_UID(uid:string):void{
    this.todoListService.getTodoes_UID(uid)
      .subscribe(todoesData => this.ngRedux.dispatch({type: AppActions.GET_TODOES, todoes:todoesData }));
  }
  //additional for ngFor trackBy
  trackByTodo(index:number,todo:Todo){
    return todo.id;
  }


}



