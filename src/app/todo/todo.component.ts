import { Component, OnInit, Input } from '@angular/core';

import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../store';
import { AppActions } from '../actions'

import { Todo } from '../model/todo';
import { TodoListService } from '../services/todo-list.service';
import { SlideInOutAnimation } from '../animations';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  animations: [SlideInOutAnimation]
})
export class TodoComponent implements OnInit {
  @Input() todo:Todo;

  isEditMode = false;
  isShowBody:boolean = false;
  animationState = 'out';

  todoEditForm:FormGroup;

  constructor(private ngRedux: NgRedux<IAppState>, private todoListService:TodoListService, private fb:FormBuilder) { }
  
  ngOnInit() {
  }

  deleteTodo(todo:Todo){
    this.todoListService.deleteTodo(todo)
    .then(o=>this.ngRedux.dispatch({type:AppActions.REMOVE_TODO}));;

  //  this.stopClickEvent();
  }

  toggleTodo(todo:Todo){
  
    if(todo.status == 1){
      todo.status = 0;
    }
    else{
      todo.status = 1;
    }
    
    todo.lastUpdated = new Date();
    
    this.todoListService.updateTodo(todo)
      .then(o=>this.ngRedux.dispatch({type:AppActions.TOGGLE_TODO}));
     // this.stopClickEvent();
  }

  updateTodo(todo:Todo){
    //this.setEditMode();//Temp need add update logic
  }

  setShowBody(){
    this.isShowBody = !this.isShowBody;

    setTimeout(()=>{
      this.animationState = this.animationState === 'out' ? 'in' : 'out';
   },0)
   
  }

  setEditMode(){
    //this.isEditMode = !this.isEditMode;
    if(this.isEditMode == true){
      this.isEditMode = false;
      this.ngRedux.dispatch({type:AppActions.CANCEL_EDIT_TODO, todo:this.todo}) 
    }else{
      this.isEditMode = true;
      this.ngRedux.dispatch({type:AppActions.EDIT_TODO, todo: Object.assign({},this.todo)})
    }
    
    //add dispaching of EDIT_TODO action and save current todo into EditedTodos array
  }

  initForm(){
    this.todoEditForm = this.fb.group({
      title:[null,[
        Validators.required
      ]
    ],
      description:[null]
    });

  
  }

  //Don't used now but need to implement some errors instead of native html
  isControlInvalid(controlName: string): boolean {
      const control = this.todoEditForm.controls[controlName];
      const result = control.invalid && control.touched;   
      return result;
    }


  stopClickEvent(e:Event){
    e.preventDefault();
    e.stopPropagation();
  }
}
