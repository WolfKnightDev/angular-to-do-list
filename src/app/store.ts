import { AppActions } from './actions'

import { Todo } from './model/todo';
import { TodoUser } from './model/todoUser';

import { TodoListService } from './services/todo-list.service'
import { User } from 'firebase/app';

export interface IAppState {
    todoes: Todo[],
    untochedTodoes:Todo[],
    user:TodoUser,
    todoesCount:number,
    activeCount:number;
    completedCount:number
}

export const INITIAL_STATE: IAppState = {
    todoes:[],
    untochedTodoes:[],
    user: null,
    todoesCount:0,
    activeCount:0,
    completedCount:0
}

export function rootReducer(state = INITIAL_STATE, action): IAppState {
    console.log(action.type);
    switch(action.type){
        case AppActions.GET_TODOES:
        return Object.assign({},state,{
            todoes:action.todoes,
            todoesCount:action.todoes.length,
            activeCount:action.todoes.filter(o=>o.status == 0).length,
            completedCount:action.todoes.filter(o=>o.status == 1).length
        })
        case AppActions.ADD_TODO:
        return state;
        case AppActions.TOGGLE_TODO:
        return state;
        case AppActions.REMOVE_TODO:
        return state;
        case AppActions.EDIT_TODO:
        return Object.assign({},state,{
            untochedTodoes:[...state.untochedTodoes.filter(o=>o.id != action.todo.id), action.todo]
        })
        case AppActions.CANCEL_EDIT_TODO:
        return Object.assign({},state,{
            todoes:[...state.todoes.filter(o=>o.id != action.todo.id),...state.untochedTodoes.filter(o=>o.id = action.todo.id)],
            untochedTodoes:state.untochedTodoes.filter(o=>o.id != action.todo.id)
        })
        case AppActions.UPDATE_TODO:
        return state;
        case AppActions.USER_SIGNIN:
        return state;
        case AppActions.USER_SIGNOUT:
        return Object.assign({},state,{
            user:null
        })
        case AppActions.USER_GETAUTH:
        return Object.assign({},state,{
               user:action.user
           })
    }
    
    return state;
}
