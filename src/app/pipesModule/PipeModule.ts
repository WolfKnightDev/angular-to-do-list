
import { NgModule }      from '@angular/core';
import { TodoListStatusFilter } from './pipes'

@NgModule({
    imports:        [],
    declarations:   [TodoListStatusFilter],
    exports:        [TodoListStatusFilter],
})

export class PipeModule {

  static forRoot() {
     return {
         ngModule: PipeModule,
         providers: [],
     };
  }
} 