import {Injectable, Pipe, PipeTransform} from '@angular/core';
import { Todo } from '../model/todo';


@Pipe({
    name:'TodoListStatusFilter'
  })

  @Injectable()
  export class TodoListStatusFilter implements PipeTransform{
  
    transform(items:any[],st:number):any{
      //console.log(items);
      return items.filter(o => o.status == st);             
    }
  }
  